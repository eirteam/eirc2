This is the base directory for EclipseIR second generation support development.
These source files are not proprietary trade secrets of EclipseIR.
The BSD license should be consulted with regard to this code.
For more information consult IP-RIGHT.txt in this directory.

Copyright (c) 2007-2015, Eclipse Identity Recognition Corporation. All rights reserved.